from controller.purchase import PurchaseController
from view.purchase import PurchaseView


def main():
    while True:
        PurchaseView.show_menu()
        option = PurchaseView.input_data("\nSeleccione una opción: ")

        if option == "1":
            phone = PurchaseView.input_data("Ingrese el número de celular: ")
            results = PurchaseController.create_purchase(phone)
            PurchaseView.show_purchases(results)
        elif option == "2":
            results = PurchaseController.view_all_purchases()
            PurchaseView.show_purchases(results)
        elif option == "3":
            purchase_id = PurchaseView.input_data(
                "Ingrese el ID de la compra: ")
            result = PurchaseController.view_purchase_by_id(purchase_id)
            PurchaseView.show_purchase(result)
        elif option == "4":
            purchase_id = PurchaseView.input_data(
                "Ingrese el ID de la compra para actualizar su estado: ")
            success, purchase = PurchaseController.update_purchase_state(
                purchase_id)
            PurchaseView.show_result(
                success, "Estado de compra actualizado correctamente.")
            PurchaseView.show_purchase(purchase)
        elif option == "5":
            purchase_id = PurchaseView.input_data(
                "Ingrese el ID de la compra a eliminar: ")
            success = PurchaseController.delete_purchase(purchase_id)
            PurchaseView.show_result(
                success, "Compra eliminada correctamente.")
        elif option == "6":
            print("Terminando la ejecución...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")


if __name__ == "__main__":
    main()
