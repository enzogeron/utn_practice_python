from model.purchase import PurchaseModel
from validations import validate_email


class PurchaseController:
    """Controlador que administra las operaciones de compra."""

    @staticmethod
    def create_purchase(phone):
        """Solicita la creación de una nueva compra."""
        email = validate_email()
        PurchaseModel.create_purchase(phone, email)
        return PurchaseModel.get_all_purchases()

    @staticmethod
    def view_all_purchases():
        """Retorna todas las compras registradas."""
        return PurchaseModel.get_all_purchases()

    @staticmethod
    def view_purchase_by_id(purchase_id):
        """Busca y retorna una compra por su ID."""
        return PurchaseModel.get_purchase_by_id(purchase_id)

    @staticmethod
    def update_purchase_state(purchase_id):
        """Actualiza el estado de una compra por su ID."""
        result = PurchaseModel.update_state_purchase(purchase_id)
        return result, PurchaseModel.get_purchase_by_id(purchase_id)

    @staticmethod
    def delete_purchase(purchase_id):
        """Elimina una compra por su ID."""
        result = PurchaseModel.delete_purchase_by_id(purchase_id)
        return result
