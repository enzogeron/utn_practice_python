import sqlite3
from common.constants import DB, PENDING_PAYMENT, ACCREDITED_PAYMENT


class PurchaseModel:
    """Administra las operaciones de la base de datos para las compras."""

    @staticmethod
    def create_table():
        """Crea la tabla de compras si no existe."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute('''
                    CREATE TABLE IF NOT EXISTS compras(
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        celular VARCHAR(50),
                        email VARCHAR(50),
                        estado TEXT CHECK(estado IN ('PENDIENTE', 'PAGADO'))
                    );
                ''')
        except sqlite3.Error as e:
            print(f"Error al crear la tabla de compras: {e}")

    @staticmethod
    def create_purchase(phone, email, state=PENDING_PAYMENT):
        """Inserta una nueva compra en la base de datos."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute("INSERT INTO compras(celular, email, estado) VALUES(?, ?, ?)",
                               (phone, email, state))
                conn.commit()
        except sqlite3.Error as e:
            print(f"Error al crear una compra: {e}")

    @staticmethod
    def get_purchase_by_id(purchase_id):
        """Obtiene una compra por su ID."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute(
                    'SELECT * FROM compras WHERE id=?', (purchase_id,))
                return cursor.fetchone()
        except sqlite3.Error as e:
            print(f"Error al obtener la compra con ID {purchase_id}: {e}")
            return None

    @staticmethod
    def update_state_purchase(purchase_id, state=ACCREDITED_PAYMENT):
        """Actualiza el estado de una compra."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute(
                    'UPDATE compras SET estado=? WHERE id=?', (state, purchase_id))
                conn.commit()
                return cursor.rowcount != 0
        except sqlite3.Error as e:
            print(f"Error al actualizar la compra con ID {purchase_id}: {e}")
            return False

    @staticmethod
    def delete_purchase_by_id(purchase_id):
        """Elimina una compra por su ID."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute(
                    'DELETE FROM compras WHERE id=?', (purchase_id,))
                conn.commit()
                return cursor.rowcount != 0
        except sqlite3.Error as e:
            print(f"Error al eliminar la compra con ID {purchase_id}: {e}")
            return False

    @staticmethod
    def get_all_purchases():
        """Retorna todas las compras registradas en la base de datos."""
        try:
            with sqlite3.connect(DB) as conn:
                cursor = conn.cursor()
                cursor.execute('SELECT * FROM compras')
                return cursor.fetchall()
        except sqlite3.Error as e:
            print("Error al obtener todas las compras: ", e)
            return []
