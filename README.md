Resumen

La aplicacion va a funcionar como un servicio web que recibe eventos de whatsapp business y los va procesando para registrar las compras que realizan los usuarios.

01/01/24 - En esta primer version se va a simular el registro de los eventos haciendo el ingreso de datos desde la cli, las validaciones de algunos campos con regex e implementar las operaciones basicas de CRUD en una tabla llamada 'COMPRAS'

14/03/24 - En esta segunda version se realizan modificaciones para agregar el patron MVC, el manejo de errores y la correcta documentacion de la aplicacion.

Configuracion

- Python 3.8.18
- SQLite3

Comandos para ejecutar la app

- python -m venv venv
- source venv/bin/activate
- python main.py