import re

def __email_regex(email):
    return bool(re.match(r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$', email))

def validate_email():
    while True:
        email = input("Ingrese el email: ")
        if __email_regex(email):
            return email
        else:
            print("Email no valido.")
