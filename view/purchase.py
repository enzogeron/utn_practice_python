class PurchaseView:
    """Vista para interactuar con el usuario en las operaciones CRUD."""

    @staticmethod
    def show_menu():
        """Muestra el menú de opciones al usuario."""
        print("\n----- MENÚ -----\n")
        print("1. Registrar una compra")
        print("2. Ver todas las compras")
        print("3. Buscar una compra por ID")
        print("4. Cambiar estado de una compra por ID")
        print("5. Eliminar una compra por ID")
        print("6. Salir")

    @staticmethod
    def show_purchases(purchases):
        """Muestra la lista de compras."""
        print("\n----- Listado de compras -----\n")
        for purchase in purchases:
            print(purchase)

    @staticmethod
    def show_purchase(purchase):
        """Muestra una compra específica."""
        if purchase:
            print("\nDetalle de la compra:\n", purchase)
        else:
            print("La compra no existe.")

    @staticmethod
    def input_data(prompt):
        """Solicita al usuario un dato."""
        return input(prompt)

    @staticmethod
    def show_result(success, message="Operación exitosa."):
        """Muestra el resultado de una operación."""
        if success:
            print(message)
        else:
            print("La operación no pudo completarse.")
